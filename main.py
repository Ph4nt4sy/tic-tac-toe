import Player
import pygame

"""
Cross = Player.Player(0)
Round = Player.Player(1)

tableTest = [
    [Cross.representation, Cross.representation, Round.representation],
    [Cross.representation, Round.representation, Round.representation],
    [Round.representation, Cross.representation, Round.representation]
]
"""


def printing(table):
    for i in range(len(table)):
        for j in range(len(table[i])):
            print('\t' + str(table[i][j]) + '\t', end='')
            if j != 2:
                print('|', end='')
        if i != 2:
            print("\n-------------------------")
    print("")


def winable(coord, table):
    # Testing the horizontal and vertical solutions
    if table[coord[0]][0] == table[coord[0]][1] and table[coord[0]][1] == table[coord[0]][2]:
        # print("horizontal")
        return True
    if table[0][coord[1]] == table[1][coord[1]] and table[1][coord[1]] == table[2][coord[1]]:
        # print("vertical")
        return True
    # Testing the diagonals
    if table[1][1] != ' ':
        if table[0][0] == table[1][1] and table[1][1] == table[2][2]:
            # print("diag")
            return True
        if table[0][2] == table[1][1] and table[1][1] == table[2][0]:
            # print("adiag")
            return True

    # If the player hasn't won this round
    return False


def round(table, tour, players, map, pos):
    # print(f"you has entered x:{pos[0]},y:{[1]}")
    screen_info = pygame.display.Info()
    cury = pos[0] // (screen_info.current_h // 3)
    curx = pos[1] // (screen_info.current_w // 3)
    # print("curx",curx)
    # print("cury",cury)
    while table[curx][cury] != ' ':
        # print("It seems that it had a problem when enter inputs, so please do it again")
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                x, y = pygame.mouse.get_pos()
                pos = [x, y]
                cury = pos[0] // (screen_info.current_h // 3)
                curx = pos[1] // (screen_info.current_w // 3)
    table[curx][cury] = [players[0], players[1]][tour % 2].representation

    map.blit([players[0], players[1]][tour % 2].surf, (
    [players[0], players[1]][tour % 2].rect.y + cury * (screen_info.current_h // 3),
    [players[0], players[1]][tour % 2].rect.x + curx * (screen_info.current_w // 3)))
    pygame.display.flip()
    return winable([curx, cury], table)


if __name__ == '__main__':
    Table = [[' ', ' ', ' '], [' ', ' ', ' '], [' ', ' ', ' ']]
    names = []
    print("Welcome to tic tac toe")
    print("Hello first player, what's your name?")
    names.append(input())
    print("And you second player?")
    names.append(input())
    Cross = Player.Player(0, names[0])
    Round = Player.Player(1, names[1])
    players = [Cross, Round]

    pygame.init()
    # initialisation of the screen
    screen = pygame.display.set_mode([600, 600])

    # fill it in white
    screen.fill((255, 255, 255))

    # Draw the first column of the grid
    pygame.draw.line(screen, (0, 0, 0), (199, 0), (199, 600))
    pygame.draw.line(screen, (0, 0, 0), (200, 0), (200, 600))
    pygame.draw.line(screen, (0, 0, 0), (201, 0), (201, 600))

    # The second one
    pygame.draw.line(screen, (0, 0, 0), (399, 0), (399, 600))
    pygame.draw.line(screen, (0, 0, 0), (400, 0), (400, 600))
    pygame.draw.line(screen, (0, 0, 0), (401, 0), (401, 600))

    # Draw the first line of the grid
    pygame.draw.line(screen, (0, 0, 0), (0, 199), (600, 199))
    pygame.draw.line(screen, (0, 0, 0), (0, 200), (600, 200))
    pygame.draw.line(screen, (0, 0, 0), (0, 201), (600, 201))

    # The second one again
    pygame.draw.line(screen, (0, 0, 0), (0, 399), (600, 399))
    pygame.draw.line(screen, (0, 0, 0), (0, 400), (600, 400))
    pygame.draw.line(screen, (0, 0, 0), (0, 401), (600, 401))

    pygame.display.flip()
    clock = pygame.time.Clock()
    tour = 0
    play = True
    while tour != 9 and play:
        # Limit the frame rate to 60 fps
        delta_time = clock.tick(60)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                play = False
            if event.type == pygame.MOUSEBUTTONDOWN:
                x, y = pygame.mouse.get_pos()
                pos = [x, y]
                play = not round(Table, tour, players, screen, pos)
                # printing(Table)
                tour += 1
        # display the update
        pygame.display.update()
    print(f"Congrats to {[Round, Cross][tour % 2].name}") if not play else print("Draw!")

pygame.quit()
