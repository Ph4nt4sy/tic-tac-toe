import pygame.sprite
import os


class Player(pygame.sprite.Sprite):
    def __init__(self, player, name):
        super(Player, self).__init__()
        self.name = name
        self.representation = ["X", "O"][player]
        self.surf = pygame.image.load([os.getcwd() + "\\data\\X.png", os.getcwd() + "\\data\\O.png"][player])
        self.surf.set_colorkey((255,255,255))
        self.rect = self.surf.get_rect()